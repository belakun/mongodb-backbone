var requestHandlers = require('./requestHandlers');

function route (_options, _handlers) {  
	var options = _options,
		handlers = _handlers,
		uri = options.uri,
		dbRegExp = /notes/,
		dbRoute = dbRegExp.exec(options.uri),
		dbRequested = dbRegExp.test(options.uri),
		fileRequested = (/\.html|\.js|\.css|\.jpg/).test(options.uri);

	console.log('uri: \'' + uri + '\' is to be handled by router!');

	if (fileRequested) {
		handlers['resouceProvider'](options);

	} else if (dbRequested) {
		options.query = (options.uri.indexOf('/', 1) !== -1) ? options.uri.slice(options.uri.indexOf('/', 1) + 1): '';
		options.uri = '/' + dbRoute[0];
		handlers[options.uri](options);

	} else if (options.uri === '/getzip') {
		handlers['/getzip'](options);
	} else { options.uri = '/index.html';
		handlers['resouceProvider'](options);
	}
}

exports.route = route;