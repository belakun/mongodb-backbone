var fs = require('fs'),
	AdmZip = require('adm-zip'),
	ObjectID = require('mongodb').ObjectID,

	MongoClient = require('mongodb').MongoClient,
	assert = require('assert'),
	url = 'mongodb://localhost:27017/notes_app';

exports.dataProvider = function (options) {	
	var collectionName = options.uri.slice(1),
		newNote = options.postData,
		requestType = options.method,
		requestMap = {
			'GET': readNotes,
			'POST': saveNote,
			'PUT': updateNote,
			'DELETE': deleteNote
		};

	requestMap[requestType](options,collectionName);
}
	
function readNotes (options, collectionName) {
	MongoClient.connect(url, function(err, db) {

	    console.log("Connected correctly to server");
	 	
	 	var collection = db.collection('notes');

	 	collection.find({}, function (err, result) {
	 		if (err) {
	 			console.log(err)
	 		} else {
	 			createResponse(options.response, {
    				code: 200,
    				body: JSON.stringify(result), 
    				contentType: 'application/json'
    			});
	 		}

	 		db.close();
	 	});
	});


}

function saveNote (options, collectionName) {
	MongoClient.connect(url, function(err, db) {
	    assert.equal(null, err);
	    console.log("Connected correctly to server");
	 	
	 	var collection = db.collection('notes');

	 	collection.insert({content: "fgsdgsdgsdg"}}, function (err, result) {

 			createResponse(options.response, {
				code: 200,
				body: JSON.stringify(result.ops[0]),
				contentType: 'application/json'
			});	    	

	    	db.close();
	 	});
	});
}

function updateNote (options, collectionName) {
	MongoClient.connect(url, function(err, db) {
	    assert.equal(null, err);
	    console.log("Connected correctly to server");
	 	
	 	var collection = db.collection('notes');

	 	collection.find({_id: new mongo.ObjectID(options.query)}, {$set: {content: "dsdgsgd", fff: "dfsdf"}}, function (err, result) {  
	 		var savedNote = result.ops;
	 		console.log(result.result);
	 			
 			createResponse(options.response, {
				code: 200,
				body: JSON.stringify(result), 
				contentType: 'application/json'
			});	    	

	    	db.close();
	 	});
	});
}

function deleteNote (options, collectionName) {
	MongoClient.connect(url, function(err, db) {
	 	var collection = db.collection('notes');	

	    assert.equal(null, err);
	    console.log("Connected correctly to server");
	 	
	 	collection.remove({'_id': mongo.ObjectID(options.query)}, function (err, result) {
	 		if (err) {
	 			console.log(err);
	 		} else {
	 			console.log(result.result);
 				createResponse(options.response, {
					code: 200,
					body: JSON.stringify(), 
					contentType: 'application/json'
				});		
	 		}
	    	db.close();
	 	});
	});
}

exports.resouceProvider = function (options) {
	var ext = options.uri.slice(options.uri.indexOf('.')),
		contentTypes = {
			'.html': 'text/html',
			'.css': 'text/css',
			'.js': 'text/javascript'
		};

	console.log('Request handler \'resouceProvider\' was called.');
	
	fs.readFile('.' + options.uri, function (err, body) {
	    if (err) {
	    	console.log('ERROR: File not found\n');

	    	createResponse(options.response, {
    			code: 404,
    			body: '404 Not found: no such file on the server!', 
    			contentType: 'text/plain'});

	    } else {
	    	console.log('SUCCESS: ' + options.uri + ' loaded\n');

	    	createResponse(options.response, {
    			code: 200,
    			body: body, 
    			contentType: contentTypes[ext]});
	    }
	});	
};

exports.getZip = function (options) {
	var zip = new AdmZip(),
		dest = "./zips/notes.zip";

	MongoClient.connect(url, function(err, db) {
	    assert.equal(null, err);
	    console.log("Connected correctly to server");
	 	
	 	var collection = db.collection('notes');

	 	collection.find({}).toArray(function (err, result) {
	 		if (err) {
	 			console.log(err)

	 		} else {
	 			console.log(result);

				zip.addFile("notes.txt", new Buffer(JSON.stringify(result)), "some comment");
				zip.writeZip(dest);


				//var notesToSend = zip.toBuffer();

				var readStream = fs.createReadStream(dest);

				readStream.on('open', function () {
	    			// This just pipes the read stream to the response object (which goes to the client)
	    			readStream.pipe(options.response);
				});

				readStream.on('error', function(err) {
    				console.log(err);
    			});
	 		}

			db.close();
	 	});
	});
};


function createResponse (response, headers) {
	response.writeHeader(headers.code, {'Content-Type': headers.contentType});  
	response.write(headers.body);  
	response.end(); 
}

exports.createResponse = createResponse;