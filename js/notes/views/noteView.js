var NoteView = Backbone.View.extend({
    tagName: 'li',
    className: 'note',
    events: {
        'click': 'removeNote'
    },
    tpl: _.template(noteTpl),
    render: function () {
        this.$el.html(this.tpl({model: this.model.attributes}));

        return this;
    },
    removeNote: function () {
        this.remove();
        this.model.destroy({silent: true});
    }
});