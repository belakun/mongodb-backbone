var NotesView = Backbone.View.extend({
    tagName: 'ul',
    className: 'notes',
    initialize: function () {
        this.notes = new Notes();

        this.notes.on('add', this.renderNote, this);

        this.notes.fetch();
    },
    render: function () {
        return this;
    },
    renderNote: function (note) {
        var view = new NoteView({model: note});
        
        this.$el.append(view.render().el);
    },
});