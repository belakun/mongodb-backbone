var NoteAddView = Backbone.View.extend({
    className: 'add-note',
    tpl: _.template(noteAddTpl)(),
    events: {
    	'click button.add': 'saveNote'
    },

    render: function () {
        this.$el.html(this.tpl);

        return this;
    },

    saveNote: function () {
    	var content = this.$('input').val();

    	var newNote = new Note({content: content});

    	newNote.on('sync', function (savedNote) {
    		mediator.trigger('noteSaved', savedNote);
    	})

    	newNote.save();
    }
});