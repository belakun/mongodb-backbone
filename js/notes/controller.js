function NotesController () {
    var $notes = $('#notes'),
        $add = $('#add-note'),
        notesView = new NotesView(),
        addView = new NoteAddView();

    mediator.on('noteSaved', updateNotes);

    $notes.append(notesView.render().el);
    $add.append(addView.render().el);

    function updateNotes (savedNote) {
        notesView.renderNote(savedNote);
    }
}