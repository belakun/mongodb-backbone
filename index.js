var server = require('./server'),
	router = require('./router'),
	requestHandlers = require('./requestHandlers'),
	handlers = {};



handlers['resouceProvider'] = requestHandlers.resouceProvider;
handlers['/notes'] = requestHandlers.dataProvider;
handlers['/globals'] = requestHandlers.dataProvider;
handlers['/getzip'] = requestHandlers.getZip;

server.run(router.route, handlers);