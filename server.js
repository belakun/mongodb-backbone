var http = require("http"),
	url = require("url"),
	qs = require('querystring');

function run (route, handlers) {
	function onRequest (request, response) {
		var options = {
			uri: url.parse(request.url).pathname,
			query: url.parse(request.url).query,
			method: request.method,
			response: response,
		},
		body = '';

		console.log("\nRequest for " + options.uri + " received. Method: " + options.method);

	    request.on('data', function (data) {
	        body += data.toString();
	    });

	    request.on('end', function () {
	        options.postData = (body) ? JSON.parse(body): '';
	        route(options,handlers);
	    });
	}

	http.createServer(onRequest).listen(8888);

	console.log('NODE.JS is running');
} 

exports.run = run;